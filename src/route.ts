import * as Router from 'koa-router';

const router = new Router();

router.get('/hello/', (ctx, next) => {
  ctx.body = 'Koa say hello';
});

router.get('/hi/', (ctx, next) => {
  ctx.body = 'Koa say hi';
});

export default router;